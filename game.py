from random import randint
#Built-in-random number generator. 

say_name = input('Hi! What is your name?')
#Ask for your name , so you respond.

month = randint(1, 12)
#gives a randon interger from 1 - 12 for month.
year = randint(1924, 2004)
#gives a random number from 1924 - 2004 for year.

print(say_name, 'were you born in', month, '/', year, '?')
#Prints a string with your resonse being first and ending with month/year.
yes_no = input('yes or no?')
#ask if yes or no, and you respond.

for num in range(4):
#Creates a for loop within range so that it repeats the number of times in range.
    if(yes_no) == 'yes':
        print('I knew it!')
        break
#if statement that prints 'I knew it!' (if resonse is equal to 'yes')

    elif(yes_no == 'no'):
        month = randint(1, 12)
        year = randint(1924, 2004)
        print('Drat! Lemme try again!')
        print(say_name, 'were you born in', month, '/', year, '?')
        yes_no = input('yes or no?')
#(Else if response is equal to 'no'), print the string and input 4 more times or until response is yes.
        
else:
    print('I have other things to do. Good Bye!!!')
#else if response is NOT 'yes' or, it has asked a total of five times, then give up, and print this.